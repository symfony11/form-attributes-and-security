<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 10; $i++) {
            $user = (new User)
                ->setEmail($faker->freeEmail)
                ->setRoles(['ROLE_USER', 'ROLE_ADMIN', ])
            ;
            $user->setPassword($this->encoder->encodePassword($user, $faker->word));

            $manager->persist($user);
        }

        $manager->flush();
    }
}

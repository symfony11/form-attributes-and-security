<?php

namespace App\DataFixtures;

use App\Entity\Admin;
use App\Entity\Article;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ArticleFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
        $admin = (new Admin)
            ->setName("root")
        ;
        $manager->persist($admin);
        $manager->flush();
        

        for ($i = 0; $i < 20; $i++) {
            $article = (new Article)
                ->setAuthor($admin)
                ->setContent("abcdfghgh")
                ->setTitle("Titre " . $i)
                ->setCreatedAt(new \DateTime())
            ;

            $manager->persist($article);
        }
        $manager->flush();
    }
}
